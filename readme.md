# Name
Effects of Copy Number Alterations on the tumour micro-environment

## Description
The code in this project is used to determine the effect of Copy Number Alterations on the
tumour micro-environment. It requires several files in a specific format to function properly.

CNAs' transcriptional effects can be analysed using Independent Component Analysis (ICA) on messenger RNA (mRNA) 
profiles ICA produces a multitude of sources, each of which contains a large number of genes. 
By genomically sorting these genes, peaks appeared in the independent components. 

The figure below shows eight sources which contain EVRs.  The x-axis contains the chromosome numbers, 
and the y-axis contains the weight of the estimated sources (explained in materials and methods). 
Each dot represents a single gene. The peaks that can be seen, which are zoomed in on, are referred to as an 
Extreme Valued Region (EVR). 

For now, it is only capable of analysing sources which contain a single EVR.

![EVR Examples](.images/EVRExamples.png)

The two images below are flow charts, explaining the flow of the project. The first flow chart depicts the steps needed 
to create the first plots, the selection of the big and small EVR groups and the reconstruction of the mRNA expression 
data. Note that the flow chart splits between sources containing one EVR and sources containing two EVRs. 
Since sources containing two EVRs were not further investigated, it shows research has ended. Sources containing a 
single EVR were further investigated 

![Workflow Part 1](.images/WorkflowPart1.png)

The second flow chart shows the process of how the correlations, P-values and confidence intervals were calculated for
the sources containing a single EVR. Also, it shows the process how the correlation plot and Manhattan plots were 
constructed. In the chart, it is noticeable that the environment was cleaned many times. This is done because the 
datasets were very large. Without cleaning the environment the computer the code was written on would often crash.

![Workflow Part 2](.images/WorkflowPart2.png)

### Data description
First and foremost it requires TCGA data. The data should, at least, have an ENTREZ_id column,
which corresponds to the gene. A CHR_MAPPING column, which corresponds to which chromosome
the gene is located on. BP_Mapping, which corresponds to the location on the chromosome in base
pairs. Finally, it should contain a column called COMPONENT, which corresponds to the different
sources. Per source, several different values of the previously mentioned columns should
be available. 

In order to calculate the EVR size, the smallest bp value for each chromosome within
each source is substracted from the largest bp value for the same chromosome within each source.

The next dataset that is required is a dataset containing TCGA sample to tumour type data.
This should, at least, contain a column called Name, which contains the TCGA name, a format of 
which a example can be seen below. Finally, it is required to possess a column called Type2.
This column should contain the type of cancer tumour associated with the TCGA sample.

![TCGA Barcode Example](.images/TCGAbarcode.png)

Next, it requires two datasets. These data sets are the mixing matrix and the estimated sources
matrix. The mixing matrix and estimated sources matrix are both a result of Independent Component
Analysis being performed on mRNA expression data. This data had to be regained. To achieve this
matrix multiplication of these datasets had been performed. The mixing matrix should contain a first
column in which the components are stored. Next, all the remaining column should contain 
the sample id, in TCGA barcode format only the hyphen should be replaced with a dot. The values
reflect the activity of each estimated source in each gene expression profile, or a genes "participation" 
in the mRNA expression pattern in a source that is independent of patterns in other sources.

The fist column of the estimated sources matrix should consist of the entrezids. The remaining
columns should contain the source numbers, but with a V added at the beginning. The values represent
the effects of independent underlying factors on the expression levels of genes.

Finally, it requires genomic mapping data. The file should contain a column called Entrezid, Bp_Mapping and
a column called Chr_Mapping. This dataset is used to determine the bp values and chromosomes for genes.
The reason this data set had to be used was because using the original TCGA data would result
in many genes not having either a bp mapping or chr mapping, meaning these genes would not be
plotted properly in the manhattan plots.

## Visuals
The code generates many different kinds of plots, some of which will be explained here. Part 1 generates 
many scatterplots and histograms. Since these speak for themselves, they wont be expained here.

### Project.Rmd
One of the plots created is called a bubbleplot. This plot shows, for the components with two EVRs, on which chromosomes
those EVRs are located. In other words it shows which combination of two chromosomes both have an EVR within a single 
component and how often this combination occurs. If a specific combination occurs five times or more they will be 
highlighted in a red colour. This threshold can easily be altered in the code and will be explained in the usage section.

![Bubble plot](.images/BubblePlot)

The bubbleplot revealed that chromosome 7 and 11 are, apparently, important as this combination
occurs often. To investigate if a possible translocation had occurred a scatterplot of the sources
containing EVRs on both chromosome 7 and 11 is created.

![Scatter plots](.images/Dotplots.png)

The selection of chromosomes is done manually. Select the two chromosomes which often have an EVR
within the same source on line 397, and the plots will be constructed automatically. Change the 
value of double_chr$Chrnr.x and double_chr$Chrnr.y respectively. Ensure that this combination 
occurs in the data, or the code will fail.

![Boxplots grouped by tumour type](.images/BoxplotPerTumourType.png)
This plot contains the values of the mixing matrix of the previously selected sources, grouped
by tumour type. It will display the "participation" of genes within these sources for each tumour 
type. In the case of this project, research concerning multiple EVRs on a single source was abandoned
after. Research was focused on sources containing a single EVR.

**The next part focuses solely on the sources containing only a single EVR.**

### ProjectPart2.Rmd

After many kinds of data manipulation and analysis have been performed a correlation plot is created.
This plot shows the correlation of the reconstructed mRNA expression against available methylation
data, for each gene in both EVR groups. When the correlation in either the big or small EVR for any
given gene exceeds 0.5 or is lower than -0.5 it will recieve a label. In addition a legend is added
to identify in which group this gene lies.

![Correlation plot](.images/CorrelationPlot.png)

To find out if the set of genes with correlation >= 0.5 or <= -0.5 occur on the same chromosome, multiple Manhattan 
plots were created. These plots contain the value of BP_mapping on the x-axis and the correlation on the y-axis. 
A plot was created for each chromosome, and the big and small EVRs are plotted in a separate plot. The image
below shows an example of the manhattan plots containing the data of the small EVRs.

![Manhattan plots of small EVRs](.images/ManhattanplotSmallEVRs.png)

The plot belows shows an example of the manhattan plots containing the information of
the big EVRs.

![Manhattan plots of big EVRs](.images/ManhattanPlotBigEVRs.png)

In the specific case of the data used during this project, chromosome 19 seemed to be important.
Therefore a manhattan plot that zoomed in on chromosome 19 had been created. However, any chromosome
can be zoomed in upon.

![Manhattan plots of Chromosome 19](.images/ManhattanPlotChromosome19.png)
 
## Installation
In order to be run correctly it is required for R to be installed. The code was written is version 4.0.3
R can be downloaded from [here](https://cran.r-project.org). Select the operating system the code
will be run on and follow the presented instructions. R will then be installed.

The used interpreter was RStudio version 1.3.1093. 
RStudio can be downloaded [here](https://rstudio.com/products/rstudio/download/#download).
Once again, select the version compatible with the operating system the code will be run on and 
follow the presented instructions.

Once RStudio is installed ensure it uses the correct R version. This can be done by selecting
tools in RStudio and selecting global options. This opens a new screen. In this screen select General.
This will show you this screen:
![Select R version](.images/SelectRVersion.png)
R version which version is currently being used by R studio. Make sure version 4.0.3 or later \
is installed.

In addition, several packages that are not available in base R are needed. These will be listed here.

ggplot2 version 3.3.2 is used to create the multitude of the plots. This package can be used to efficiently 
make easy to understand plots. It is installed by running the following code in the R console screen:
```bash
install.packages("ggplot2")
```

dplyr version 1.0.2 is a package that helps with data manipulation. dplyr can be installed in the following way
```
install.packages("dplyr")
```

The data.table package version 1.13.2 is used to read files more efficiently into R, and efficiently write out results 
to new files. 
```bash
install.packages("dplyr")
```

plyr version 1.8.6 is a package that allows the user to split a problem in manageable pieces. 
in this project it is used to calculate the correlation between mRNA expression and methyl data 
for individual genes by providing it in a specific way to a custom function. The package is installed
by running the following code:
```bash
install.packages("plyr")
```

ggrepel version 0.8.2 is a package that allows ggplot to repel overlapping labels, so that all labels
are clear and easy to read. It is installed in the following way:
```bash
install.packages("ggrepel")
```

Finally, a library containing genes and their respective gene symbols is also required. 
```bash
install.packages("EnsDb.Hsapiens.v86")
```

These packages are automatically loaded when running the code, but they are not automatically installed.
Ensure that they are manually installed or the code will not be able to complete.

## Usage

### Project.Rmd
All code is run automatically, needing little input from the user. However, there are a few things that require
user input. This being the working directory, the big/small EVR cutoff and the file pathways. The working directory is defined
in the first code chunk, and can be identified by by:
```bash
knitr::opts_knit$set(root.dir = "...")
```
Put the working directory in place of the dots, and ensure the pathways of the data files are identical to how
they are defined here:

First, TCGA data is required. This should consist of a ENTREZID column, a CHR_Mapping column, a BP_Mapping column 
and a component column. It is used in Project.Rmd. The located of this file can be changed on line 32.

This data is used to calculate the EVR sizes by subtracting the lowest BP value per component from the largest
BP value per component. In some cases a single component can contain two or more EVRs. In that case the EVR size
is calculated per chromosome. The pathway is defined on line 32. Ensure the variable name is not changed as this will
cause the code to not run properly. After this many different kind of plots will be created. 

The mixing matrix and sample tumour data need to be loaded in next. The mixing matrix is defined on line 344 and
the sample tumour data is defined on line 345. The two datafiles are used to create the dot plots of
the sources which contain a EVRs on two specific chromosomes, and to create the boxplots of tumour types.
The chromosomal combination needs to be manually changed. This is done on line 349. Note this is done
based on the information provided by the variable double_chr_count. First manually search for the
chromosome combination you are interested in. This data frame will reveal the count of each specific
chromosome combination. Currently, this part can only properly handle a threshold that is only met once.
The plots also use manual breaks in the plots, that being seven and eleven. 
These should be changed to match the chromosomes of the chromosomal combination that occurs most often. 
This is done on line 372, 383 and 398. As previously mentioned research concerning multiple EVR contained
within a single source was abandoned after this because it did not produce important results.

Finally, the boxplots of the tumour types are created automatically, requiring no manual input.

Now the cut off needs to be decided. Change the cut_off variable on line 475 to a value between 1 and 100. This value will 
be provided to the quantile function. This function will determine a value based on the number provided.
It will also receive all relative sizes of the sources containing one EVR. Using this information
it will determine the cut off at which x per cent of the EVRs is considered small, and one 1 - x per
cent of the EVRs will be considered big, where x is the number provided. In the case of this project
a value of 20 was provided, resulting in a cut off of 0.1692. 

Then, the original TCGA data is divided into two files. One file contains the sources with one EVR
which are considered big and the other contains the sources with one EVR which are considered small.
Next, the mixing matrix and estimated sources matrix are needed. The estimated sources matrix is defined on line 500. Then, by using the sources, a subset is made of both the 
mixing matrix and estimated sources matrix in the same way.  Finally, matrix multiplication is used
by multiplying the rows of the estimated sources matrix by the columns of the mixing matrix. 

The files will be automatically written to a relative path;
``
Datasets/Part2/TestData/filename
``
It is advised not to change this, as part 2 of the project expects the data to be saved in this
location.

Now that the mRNA expression is recreated, further analyses were done in ProjectPart2.Rmd

### ProjectPart2.Rmd
Part2 is mostly automatic as well, requiring little input. However, A few variables require manual input.  
First, it requires the TCGA sample to tumour type data and the methylation data. The TCGA tumour 
type data is defined on line 61 and the methylation data is defined on line 62.
This part also requires a cut off to be defined, on line 92. Ensure that this is the same value as the one used
in the first part. This part creates several files and writes them to a pre defined location.
If the locations of the files in the first part are not changed, the entire script will require no
further manual input. A test to investigate the validity of custom functions is performed. This starts at line
166 and ends at 206. This chunk requires visual confirmation of the validity of the custom functions.
The entrezid that are checked can, potentially, be changed.

### ProjectPart3
Finally, analysis involving cibersort has been performed as well.
However, this was quickly abandoned as this produced no notable results that would
add to the already existing results. This was done to analyse the immune microenvironment, rather
than the tumour microenvironment. The code does, once again, require a cut off to be provided. This
is done on line 31. The cibersort data is provided on line 33. Finally, the result of the melt function
performed on the big and small EVR mRNA expression data need to be provided on lint 35 and 38 respectively.
If the standard writing locations were used, only the cibersort location needs to be (potentially) changed.
The cibersort data should consist of several samples, and contain any number of rows.
Since a columns number is used to select cells against which to perform correlation, make sure that
these match. In this case columns 1,4,5,8,10,27 are selected, with column one containing the first 12 characters
of the TCGA sample barcode. The other columns include CD8 cells, CD4 cells, regulatory T cells and a
absolute score. The last five columns can be anything the user wishes, as long as it contains cibersort data.

# Licence
[MIT License](https://choosealicense.com/licenses/mit/)

Copyright © 2021 Rik van de Pol  
(Student Bioinformatics at the Hanze University Applied Sciences)  

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

